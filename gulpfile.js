var gulp = require('gulp');
var connect = require('gulp-connect'); // Run a local dev server
var open = require('gulp-open') // Open a URL in a web browser
var browserify = require('browserify'); // Bundles JS
var babelify = require('babelify'); // Transform React JSX to JS
var source = require('vinyl-source-stream'); // Use conventional text streams with Gulp
var concat = require('gulp-concat'); // Concatenates files
var babel = require('gulp-babel');
var history = require('connect-history-api-fallback'); //Config history fallback


var config = {
    port: 3000,
    devBaseUrl: 'http://localhost',
    paths: {
        html: './src/*html',
        jsx: './src/**/*.jsx',
        js: './src/**/*.js',
        css: [
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
            'node_modules/toastr/build/toastr.css'            
        ],
        dist: './dist',
        main: './src/index.jsx'
    }
}

//Start a local development server
gulp.task('connect', function() {
    connect.server({
        root: ['dist'],
        port: config.port,
        base: config.devBaseUrl,
        livereload: true,
        fallback: 'dist/index.html'
    });
});

gulp.task('open', ['connect'], function() {
    gulp.src('dist/index.html').pipe(open({ uri: config.devBaseUrl + ':' + config.port + '/' }));
});

gulp.task('build-react', function() {
    return browserify({ entries: config.paths.main, extensions: ['.jsx', '.js'], debug: true })
        .transform('babelify', { presets: ['es2015', 'react'] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest(config.paths.dist + '/scripts'))
        .pipe(connect.reload());;
});

// gulp.task('build-js', function() {
//     return gulp.src(config.paths.js)
//         .pipe(babel())
//         .pipe(concat('bundle-js.js'))
//         .pipe(gulp.dest(config.paths.dist + '/scripts'));
// });

gulp.task('html', function() {
    gulp.src(config.paths.html)
        .pipe(gulp.dest(config.paths.dist))
        .pipe(connect.reload());
});

gulp.task('css', function() {
    gulp.src(config.paths.css)
        .pipe(concat('bundle.css'))
        .pipe(gulp.dest(config.paths.dist + '/css'));
});

gulp.task('watch', ['build-react'], function() {
    gulp.watch(config.paths.html, ['html']);
    gulp.watch(config.paths.jsx, ['build-react']);
    gulp.watch(config.paths.js, ['build-react']);
});

gulp.task('default', ['html', 'css', 'build-react', 'open', 'watch']);