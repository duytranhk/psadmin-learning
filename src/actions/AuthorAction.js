"use strict";

import Dispatcher from '../dispatcher/AppDispatcher';
import AuthorApi from '../api/AuthorApi';
import ActionType from '../constants/ActionType';

class AuthorAction {
    createAuthor(author) {
        var newAuthor = AuthorApi.saveAuthor(author);

        // Hey Dispatcher, go tell all the stores that an author was just created
        Dispatcher.dispatch({
            actionType: ActionType.CREATE_AUTHOR,
            author: newAuthor
        });
    }
};

export default AuthorAction;