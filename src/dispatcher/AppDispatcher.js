// Singleton - one Dispatcher per app
import { Dispatcher } from 'flux';

export default new Dispatcher();