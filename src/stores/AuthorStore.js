"use strict";

import Dispatcher from '../dispatcher/AppDispatcher';
import ActionType from '../constants/ActionType';
import { EventEmitter } from 'events';
import assign from 'object-assign';
import _ from 'lodash';

var _authors = [];
const CHANGE_EVENT = 'change';
var AuthorStore = assign({}, EventEmitter.prototype, {
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },
    getAllAuthor: function() {
        return _authors;
    },
    getAuthorById: function(id) {
        return _.find(_authors, { id: id });
    }
});

Dispatcher.register(action => {
    switch (action.actionType) {
        case ActionType.CREATE_AUTHOR:
            _authors.push(action.author);
            AuthorStore.emitChange();
    }
});

export default AuthorStore;