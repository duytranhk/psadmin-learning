import React, {Component} from 'react';

class NotFoundPage extends Component {
    constructor(){
        super();
    }
    render() {
        return(
            <div>
                <h1>Page Not Found</h1>
            </div>
        );
    }
}

export default NotFoundPage;