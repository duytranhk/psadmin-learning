"use strict";
import React, {Component}  from 'react';

class AboutPage extends Component {
    constructor(){
        super()
    }
    static willTransitionTo(transition, params, query, callback){
        if(!confirm('Bla bla bla')){
            transition.abort();
        } else {
            callback();
        }
    }
    render(){
        return(
            <div>
                <h1>About</h1>
                <p>
                    This is application uses the following technologies
                </p>
                    <ul>
                        <li>React</li>
                        <li>ReactRoute</li>
                        <li>NodeJS</li>
                        <li>Flux</li>
                    </ul>
                
            </div>
        );
    }
}
export default AboutPage;