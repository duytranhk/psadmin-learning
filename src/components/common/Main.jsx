import React, {Component} from 'react';
import {Link} from 'react-router';

class Main extends Component {
    render(){
        return(
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <a href="#" className="navbar-brand">
                            <span>MyApp</span>
                        </a>
                        <ul className="nav navbar-nav">
                            <li><Link to="/" activeClassName="active">Home</Link></li>
                            <li><Link to="authors" activeClassName="active">Authors</Link></li>
                            <li><Link to="about" activeClassName="active">About</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Main;