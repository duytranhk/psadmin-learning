import React, {Component} from 'react';
import {Link} from 'react-router';

class AuthorList extends Component{
    constructor(props){
        super(props);
        this.state = {authors: this.props.authors};
    }
    render(){
        var createAuthorRow = this.state.authors.map((author) => {
            return(
                <tr key={author.id}>
                    <td><Link to={{ pathname:"/author", query: {id: author.id}}}>{author.id}</Link></td>
                    <td>{author.firstName} {author.lastName}</td>
                </tr>
            );
        });
        return(
            <div>
                <table className="table table-responsive table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {createAuthorRow}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default AuthorList;