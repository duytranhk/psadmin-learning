"use strict"
import React, {Component} from 'react';
import AuthorForm from './AuthorForm.jsx';
import Router, {browserHistory, Lifecycle } from 'react-router'; 
import AuthorStore from '../../stores/AuthorStore';
import AuthorAction from '../../actions/AuthorAction';
import toastr from 'toastr';
import reactMixin from 'react-mixin'; // support mixin

class ManageAuthorPage extends Component {
    constructor(){
        super();
        this.state = {
            author: {id:'', firstName:'', lastName:''},
            errors: {},
            dirty: false
        }
        this.setAuthorState = this.setAuthorState.bind(this);
        this.saveAuthor = this.saveAuthor.bind(this);
        this.routerWillLeave = this.routerWillLeave.bind(this);
        this.componentWillMount = this.componentWillMount.bind(this);
    }

    // invoke when leave page - react router
    routerWillLeave (nextState, router) {
        if(this.state.dirty && !confirm('Leave without saving? ')){
            router.abort();
        }
    }
    
    // WillMount = OnLoad
    componentWillMount(){
        var authorId = this.props.location.query.id;
        if(authorId){
            this.setState({author: AuthorStore.getAuthorById(authorId)});
        }
    }

    setAuthorState(event){
        this.setState({dirty:true});
        var field = event.target.name;
        var value = event.target.value;
        this.state.author[field] = value;
        return this.setState({author: this.state.author});
    }
    authorFormIsValid(){
        var formIsValid = true;
        this.state.errors = {} // clear previous error

        if(this.state.author.firstName.length < 3){
            this.state.errors.firstName = "First Name must be at least 3 characters"
            formIsValid = false;
        }
          if(this.state.author.lastName.length < 3){
            this.state.errors.lastName = "Last Name must be at least 3 characters"
            formIsValid = false;
        }

        this.setState({errors: this.state.errors});
        return formIsValid;
    }

    saveAuthor(event){
        event.preventDefault();
        if(!this.authorFormIsValid()){
            return;
        }
        
        AuthorAction.createAuthor(this.state.author);
        this.setState({dirty:false});
        toastr.success("Author saved.")
        browserHistory.push('authors');
    }

    render(){
        return(
            <div>
                <h1>Manage Author</h1>
                <AuthorForm author={this.state.author}
                            onChange={this.setAuthorState}
                            onSave={this.saveAuthor}
                            errors={this.state.errors}/>
            </div>
        );
    }

}

reactMixin.onClass(ManageAuthorPage, Lifecycle);
export default ManageAuthorPage;