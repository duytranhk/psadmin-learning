"use strict";

import React, {Component} from 'react';
import TextInput from '../common/TextInput.jsx'

class AuthorForm extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <form>
                <TextInput
                    name="firstName"
                    label="First Name"
                    value={this.props.author.firstName}
                    onChange={this.props.onChange}
                    placeholder="First Name"
                    error={this.props.errors.firstName}/>
                
                <TextInput
                    name="lastName"
                    label="Last Name"
                    value={this.props.author.lastName}
                    onChange={this.props.onChange}
                    placeholder="Last Name"
                    error={this.props.errors.lastName}/>

                <input type="submit" value="Save" className="btn btn-success" onClick={this.props.onSave}/>
            </form>
        );
    }
}
 
AuthorForm.propTypes = {
    author:React.PropTypes.object.isRequired,
    onSave: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
    errors: React.PropTypes.object
}
export default AuthorForm;