import React, {Component} from 'react';
import Router, {Link} from 'react-router';
import AuthorApi from '../../api/AuthorApi';
import AuthorList from './AuthorList.jsx';

class AuthorPage extends Component{
    constructor(){
        super();
        this.state = {authors:AuthorApi.getAllAuthors()}
    }
    render(){
        return(
            <div>
                <h1>Authors</h1>
                <Link to="author" className="btn btn-info">Add Author</Link>
                <AuthorList authors={this.state.authors}/>
            </div>
        );
    }
}

export default AuthorPage;