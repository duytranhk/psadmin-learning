"use strict";
import keymirror from 'fbjs/lib/keymirror';

module.exports = keymirror({
    CREATE_AUTHOR: null
});