module.exports = {
    authors: [{
            id: "99742dc0-ef82-5380-be36-0bc3a26c8751",
            firstName: 'Erik',
            lastName: 'Jennings'
        },
        {
            id: "886841e2-cc7e-54b8-9f40-bcd4e6dd778d",
            firstName: 'Edgar',
            lastName: 'Smith'
        },
        {
            id: "e60feeb8-2e23-52cd-a518-762c1e1fa5bd",
            firstName: 'James',
            lastName: 'Dennis'
        },
        {
            id: "e01c2f1e-4a43-55f6-b0d9-f733fb21497a",
            firstName: 'Polly',
            lastName: 'Newman'
        },
        {
            id: "31dbfad0-f93e-5c6c-b25c-7dae3d0b656b",
            firstName: 'Lydia',
            lastName: 'Wagner'
        },
        {
            id: "4d7f1c34-b312-5089-82a5-37ffa57a0654",
            firstName: 'Herbert',
            lastName: 'Conner'
        },
    ]
}