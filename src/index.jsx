import React, { Component } from 'react';
import { render } from 'react-dom';
import jquery from 'jquery';
// Import routing components
import {Router, Route, IndexRoute, browserHistory, Redirect} from 'react-router';
import Main from './components/common/Main.jsx';
import HomePage from './components/common/HomePage.jsx';
import AboutPage from './components/common/AboutPage.jsx';
import NotFoundPage from './components/common/NotFoundPage';
import AuthorPage from './components/authors/AuthorPage.jsx';
import ManageAuthorPage from './components/authors/MangeAuthorPage.jsx';


render(
    <Router history={browserHistory}>
        <Route  path="/" component={Main}>
            <IndexRoute component={HomePage}/>
            <Route path="authors" component={AuthorPage}/>
            <Route path="author" component={ManageAuthorPage}/>
            <Route path="author/:id" component={ManageAuthorPage}/>            
            <Route path="about" component={AboutPage} />
            <Route path="*" component={NotFoundPage}/>
            <Redirect from="about-us" to="about"/>
            <Redirect from="awthurs" to="authors"/>
            <Redirect from="about/*" to="about"/>
        </Route>
    </Router>,
    document.getElementById('root')
);